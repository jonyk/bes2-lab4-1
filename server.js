
var port = 3000;
var info = "Patient monitoring server PoC v0.1";

var http = require('http');
var express = require('express');
var WebSocketServer = require('websocket').server;

var app = express();

// configure Express
app.use(express.static(__dirname + '/public'));

app.get('/', function(req, res) {
  res.send(__dirname + "/public/index.html");
});

var http_server;

try {
  http_server = http.createServer(app).listen(port);
} catch (err) {
  console.log('Error:\n  ' + err.name + ':' + err.message + '\n  ' + err.stack);  
  process.exit(1);
} 

var ws_server = new WebSocketServer({
  httpServer: http_server,
  // Firefox 7 alpha has a bug that drops the
  // connection on large fragmented messages
  fragmentOutgoingMessages: false
});

var connections = [];

function broadcast(message) {
  connections.forEach(function(destination) {
    destination.sendUTF(JSON.stringify(message));
  });
}

ws_server.on('request', function(request) {
	
  console.log('request.origin', request.origin);
  
  var connection = request.accept('ws', request.origin);
  connections.push(connection);

  console.log(connection.remoteAddress + " connected - Protocol Version " + connection.webSocketVersion);

  // Send all the existing canvas commands to the new client
  connection.sendUTF(JSON.stringify({ type: "hello", msg: info }));

  // Handle closed connections
  connection.on('close', function() {
    console.log(connection.remoteAddress + " disconnected");
    // remove the connection from the pool
    var index = connections.indexOf(connection);
    if (index !== -1) {
      connections.splice(index, 1);
    }
  });

  // Handle incoming messages
  connection.on('message', function(message) {
    if (message.type === 'utf8') {
      try {
        var payload = JSON.parse(message.utf8Data);
        if (payload.type === 'input_stream') {
          // rebroadcast stream to all clients
          broadcast({ type: 'stream', data: payload.data });
        }
      } catch (e) {
        // do nothing if there's an error.
      }
    }
  });
});

console.log("Test patient monitoring server ready!");
